## Mettre en place Composer
Composer permet de gérer les dépendances d'un projet, pour l'installer il suffit de suivre les instructions se trouvant à https://getcomposer.org/download/ . Il est très utile de l'installer en global, afin de pouvoir utiliser Composer pour plusieurs projets.

## Installer le Framework
Avec Composer, l'installation de Laravel s'effectue grâce à la commande `composer global require laravel/installer`.

La commande `php artisan serve` permet de configuer un serveur live avec le module artisan.

Sur la documentation d'installation on trouve les fichiers de configurations Apache/NginX pour la réécriture d'URL.

## Configurer son serveur local
Il est possible d'avoir une meilleure configuration pour le développement local en créant un Virtual Host. Il est nécessaire de redémarrer W|M|X|L.A.M.P.

## Définir l'architecture du Framework

- Le dossier **app** contient la majeure partie du code de l'application ;
- le dossier **bootstrap** contient le fichier de lancement de Laravel ;
- le dossier **config** qui permet de configurer le comportement de Laravel (E-mail etc) ;
- le dossier **database** qui contient les données nécessaires à une migration par exemple ;
- le dossier **public** est le dossier appelé lorsque l'on accède au site, il contient notamment le fichier `index.php` qui est le premier appelé.
- le dossier **ressources** qui contient les fichiers JS, les images, les langues, etc.
- le dossier **routes** qui contient les routes auxquels l'application répond;
- le dossier **storage** qui contient des fichiers de caches, des logs des versions précompilé;
- le dossier **vendor** qui contient toutes les extensions de Laravel ;
- le fichier `.env` qui configure l'application

## Configurer pour le développement ou la production
### Configuration pour le développement
Il est est préférable d'activer le paramètre `APP_DEBUG` dans le `.env`. 

Il est intéressant d'utiliser la debug bar à partir du github *laravel-debugbar*.



## Comprendre la notion de Framework
C'est un ensemble de fonctionnalités, de code générique regroupés au sein d'une bibliothèque logicielle et respectant des conventions et des normes.

Il existe différents types de Framework :

- Framework d'infrastructure système, permet de créer des OS ;
- Framework middleware, permet de fédérer des logiciels écrit dans différents langages ;
- Framework d'entreprise, spécifique à une entreprise/à un secteur ;
- Framework de gestion de contenu.

La plupart des frameworks utilisent la POO, l'héritage et permet d'avoir moins de redondance de code.

Exemple de Framework Open Source :

- Laravel ;
- Symfony ;
- Zend ;
- Slim ;
- CodeIgniter ;

## Prendre en main les outils du Framework
Avantage d'un Framework Open Source :

- Trouver facilement des développeurs ;
- Il est régulièrement plus à jour ;
- Des modules mis à disposition de la part des autres développeurs.

Laravel contient de nombreuses bibliothèques ainsi qu'une documentation détaillée. Le framework permet également une authentifaction et autorisation par hiérarchie ainsi qu'une très bonne gestion du cache. La ligne de commande est également très apréciable car elle permet de générer beaucoup de code pour le squellette de site. 

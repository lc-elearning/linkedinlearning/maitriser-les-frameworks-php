## Définir le vocabulaire
Un objet est un élément du code qui possède des attributs (les variables) et des méthodes (les fonctions). Chaque objet est une instance de classe, une classe un modèle que l'objet implémente (classe poisson, objet poisson bleu avec une nageoire).

L'héritage simple avec `Extends` permet de partager simplement des attributs et des méthodes.

Les traits permettent de réutiliser du code, ils permettent de regrouper des fonctionnalités utilisées dans plusieurs classes.

Les classes abstraites permettent de créer des classes qui ne sont pas implémentable et qui servent à définir des propriétés que d'autres classes font hériter et définir.

Les interfaces permettent de vérifier que les objets possédent différentes fonctionnalités.

## Comprendre l'ORM Eloquent
L'Object Relational Maping est ensemble de classe permettant de manipuler des bases de donées.

## Découvrir le MVC
Le pattern Modéle Vue Contrôleur permet structurer un projet et de stabiliser son évolution.

- **La couche Modèle** permet de gérer les données, de les structurer et de les récupérer pour ensuite les envoyer au contrôleur.
- **La couche Vue** permet de gérer l'affichage et de transformer les données ;
- **La coouche Contrôleur** permet de gérer les demandes de l'utilisateur, manipule les données grâce aux modèles et d'envoyer les données aux vues.

## Aborder les normes de développement
Les fichier `.env` permettent de configurer le projet Laravel. Le paramètre `APP_KEY` permet de définir une chaine de caractère qui va permettre d'encoder les mots de passes et les cookies. Elle doit être différentes en fonction de la plateforme (local, dev, production). Il est mieux de ne pas partager le `.end` via git par exmeple.

Pour un modèle le nom doit être au singulier, il va étendre la classe `Model`., idem pour le Contrôleur. Le nom de la classe doit être idnetique au nom du fichier.

## Obtenir de l'aide et accéder à la documentation
La documentation permet de suivre les fonctionnalités de la version la plus récente mais également celles des versions précédentes. Elle peut se trouver à https://laravel.com/docs/ , il également intéressant de suivre le tag Laravel sur StackOverflow.

